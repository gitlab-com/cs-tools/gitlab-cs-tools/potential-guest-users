# Potential Guest users

Identify users that **could potentially** be Guest users (and thus free users on an Ultimate subscription) based on their contributions.

**Please view the potential Guest users as rough estimate, based on those users not pushing, creating or approving merge requests and not destroying environments, i.e. not actively performing the core development tasks. It is possible that users perform none of these tasks and still require a higher role, for example to trigger pipelines or manage configuration or members.**

## Personally identifyable information (PII) warning

This script extracts user data (usernames, emails) from GitLab and creates reports. That data is PII and has to be protected. Don't send reports created by this script to other people (i.e. your sales rep, your CSM, support etc). If you need advice how to analyse the results, only send summary statistics or anonymized files that don't contain PII.

# Documentation

This script will, for a configurable amount of days (the *checked period*), check user activity. It generates a number of reports. Users will only appear in exactly one of these reports.:

* `inactive_users.csv`, containing users that have not logged in during the checked period. They can be deactivated to not further consume a seat.
* `potential_guest_users.csv`, containing users that have not performed any tracked actions incompatible with the `Guest` permission.
  * The file contains some additional metadata to filter potential Guests more easily. The file will contain information on the highest role (10 to 50: Guest to Owner) the user has outside their personal namespace. It also contains how many projects and groups the user has Owner / Maintainer role in (outside their personal namespace), as well as how many personal projects the user has.
  * Because Maintainers and Owners often have further responsibilities, they are less likely targets for Guest users. However, being Owner of a personal project is not necessarily incompatible with being a Guest user.
  * These statistics can help you target, for example, Developer users that only perform Guest actions but have personal projects where they are Owners.

On self-managed GitLab, it will further generate:

* `no_membership_users.csv`, containing users that are not members of any groups or projects. These are considered "Guest" users on an Ultimate license, as they don't have any memberships higher than "Guest" level by definition. The report is based on [user's memberships](https://docs.gitlab.com/ee/api/users.html#user-memberships-admin-only). If a user is not member of any group or project, they will be added to this list. They will not appear on `potential_guest_users.csv`.
* `non_billable_users.csv`, containing non-billable users that do have memberships, thus they are existing `Guest` users on Ultimate.

On SaaS, it will further genrate:

* `private_profile_users.csv`, containing users whose profile is private and thus can't be checked for guest status or last activity.

`potential_guest_users.csv` is based on the [user contribution events](https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events), which tracks certain [actions](https://docs.gitlab.com/ee/user/index.html#user-contribution-events) of users. Many of these actions require higher than [Guest permissions](https://docs.gitlab.com/ee/user/permissions.html), for example pushing to repos or opening/approving MRs.

If during the checked period, no such events are found, the user is marked as potential Guest and added to `potential_guest_users.csv`.

**Note** that not all events are tracked, so `potential_guest_users.csv` may contain users executing actions that do require higher permissions.
For example, environment actions, viewing dashboards, cancelling jobs, managing variables, viewing merge requests etc are all not tracked as events and thus can not be checked by this report.

## Configuration

### Self-managed

- Export / fork repository.
- Add a GitLab **admin** API token to CI/CD variables named `GITLAB_TOKEN`. Make sure it is "masked".
This token will be used to query the API for group and project members.
The token **must** be an admin token to be able to list all users and access their current sign in.
- Add your GitLab instance URL to CI/CD variables named `GITLAB_URL`.
- Configure `CHECK_PERIOD`: This is the period in days that users contributions are checked for. Defaults to 180 days
- Notice the Job is tagged `local` for testing purposes. Make sure your runners pick it up.

### GitLab.com (SaaS)

- Export / fork repository.
- Add a GitLab **group owner** API token (of the group you want to check) to CI/CD variables named `GIT_TOKEN`. Make sure it is "masked". This token will be used to query the API for group and project members. The token **must** be a group owner token to be able to get all group and project memberships of your group.
- Note that `GITLAB_TOKEN` is not used for the SaaS jobs, thus no admin token is needed here
- Configure `SAAS_GROUP`, the path or ID of the group you want to query
- Configure `CHECK_PERIOD`: This is the period in days that users contributions are checked for. Defaults to 180 days

## Usage

### Self-managed

`pip3 install -r requirements.txt`
`python3 identify-guest-users.py $GITLAB_URL $GITLAB_TOKEN [--check_period $CHECK_PERIOD]`

By default, the script only prints summary statistics. In order to get actual user lists, start the script with

`python3 identify-guest-users.py $GITLAB_URL $GITLAB_TOKEN [--check_period $CHECK_PERIOD] [--include_personal_data]`

### GitLab.com (SaaS)

`pip3 install -r requirements.txt`
`python3 identify-guest-users-saas.py $GIT_TOKEN $MEMBERS_CSV [--check_period $CHECK_PERIOD]`

`$MEMBERS_CSV` refers to the output file of the [Group Member Report](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/gitlab-group-member-report). So you have to run that report first, take its output CSV and put it into this script.

By default, the script only prints summary statistics. In order to get actual user lists, start the script with

`python3 identify-guest-users-saas.py $GIT_TOKEN $MEMBERS_CSV [--check_period $CHECK_PERIOD] [--include_personal_data]`

## Runtime and rate limiting

This project will perform a large number of API calls. For each user, all memberships and contributions in the check period will be requested and evaluated. As they come paginated in 100 per page and assuming an average 20 contributions per business day, that will be 26 pages of contributions per person to crawl and evaluate at the maximum. Multiply this by the number of users in the organisation and you get a lot of traffic on the API.

The GitLab API is [rate limited](https://docs.gitlab.com/ee/security/rate_limits.html) on GitLab.com and optionally on self-managed. You can look at the specific rate limits [here](https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlabcom-specific-rate-limits). This script will [obey rate limits](https://python-gitlab.readthedocs.io/en/stable/api-usage-advanced.html#rate-limits) by checking the `Retry-After` headers when getting 429 responses automatically. In absence of the headers, it will perform a maximum of 10 retries with exponential backoff. This exponential backoff can cause some additional wait times in certain situations.

Thus, the runtime of this script can be very long, doing API calls and potentially waiting for rate limits, especially if you have many users.

## Disclaimer

This script is provided for educational purposes. It is **not supported by GitLab**. However, you can create an issue if you need help or propose a Merge Request. This script only reads data via the GitLab API and does not perform write actions. For self-managed, this script requires an admin token, which is a very powerful credential. For SaaS, this script requires a group owner token, which is very powerful on your group as well. Make sure to rotate/expire the token regularly.

This script produces data file artifacts containing user names and (on self-managed) emails, i.e. personally identifiable information (PII). Please use this script and its outputs with caution and in accordance to your local data privacy regulations.

